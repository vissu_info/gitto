package com.vissu.accesstogit.access2git;

import android.support.test.InstrumentationRegistry;

import com.vissu.accesstogit.access2git.models.response.BaseResponse;
import com.vissu.accesstogit.access2git.models.response.Repo;
import com.vissu.accesstogit.access2git.models.response.ResultsResponse;
import com.vissu.accesstogit.access2git.models.response.User;
import com.vissu.accesstogit.access2git.network.ApiClient;

import junit.framework.Assert;

import org.junit.Test;

import java.util.List;

import rx.functions.Action1;

/**
 * Created by vissu on 10/14/16.
 */

public class ApiClientTest {

    private static final String DUMMY_AUTHENTICATION = "Basic UmFpbjE4MTA6MTIzNDU2Nzhh";
    private static final String DUMMY_USERNAME = "rain1810";
    private static final String SEARCH_TERM = "android";

    @Test
    public void testRequestAuthentication() throws Exception {
        ApiClient.getInstance(InstrumentationRegistry.getTargetContext()).requestAuthentication(DUMMY_AUTHENTICATION)
                .subscribe(new Action1<BaseResponse<User>>() {
                    @Override
                    public void call(BaseResponse<User> response) {
                        Assert.assertNotNull(response);
                        Assert.assertNotNull(response.data);
                        Assert.assertEquals(response.data.username, DUMMY_USERNAME);
                    }
                });
    }

    @Test
    public void testSearchRepos() throws Exception {
        ApiClient.getInstance(InstrumentationRegistry.getTargetContext()).requestSearchRepos(SEARCH_TERM)
                .subscribe(new Action1<BaseResponse<ResultsResponse>>() {
                    @Override
                    public void call(BaseResponse<ResultsResponse> repos) {
                        Assert.assertNotNull(repos);
                        Assert.assertNotNull(repos.data);
                        Assert.assertNotNull(repos.data.repos);
                        Assert.assertTrue(repos.data.repos.size() > 0);
                    }
                });
    }

    @Test
    public void testRequestStarredRepos() throws Exception {
        ApiClient.getInstance(InstrumentationRegistry.getTargetContext()).requestStarredRepos()
                .subscribe(new Action1<BaseResponse<List<Repo>>>() {
                    @Override
                    public void call(BaseResponse<List<Repo>> repos) {
                        Assert.assertNotNull(repos);
                        Assert.assertNotNull(repos.data);
                        Assert.assertTrue(repos.data.size() > 0);
                    }
                });
    }

}
