package com.vissu.accesstogit.access2git.binding;

import android.databinding.BaseObservable;
import android.text.TextUtils;

/**
 * Created by vissu on 10/18/16.
 */

public class BindableString extends BaseObservable {
    private String value;

    public String get() {
        return value != null ? value : "";
    }

    public void set(String value) {
        if (this.value == null || !this.value.equals(value)) {
            this.value = value;
            notifyChange();
        }
    }

    public boolean isEmpty() {
        return TextUtils.isEmpty(value);
    }
}