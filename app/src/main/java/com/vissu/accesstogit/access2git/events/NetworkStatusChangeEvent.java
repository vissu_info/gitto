package com.vissu.accesstogit.access2git.events;

/**
 * Created by vissu on 10/19/16.
 */

public class NetworkStatusChangeEvent {

    public boolean isConnected = true;

    public NetworkStatusChangeEvent() {}

    public NetworkStatusChangeEvent(boolean isconnected) {
        this.isConnected = isConnected;
    }
}

