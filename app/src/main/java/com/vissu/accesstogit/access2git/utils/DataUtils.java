package com.vissu.accesstogit.access2git.utils;

import android.util.Base64;

/**
 * Created by vissu on 10/13/16.
 */

public class DataUtils {

    public static String getBasicAuth(String username, String password) {
        String encodedCredentials = Base64.encodeToString(String.format("%s:%s", username, password).getBytes(), Base64.NO_WRAP);
        return String.format("Basic %s", encodedCredentials);
    }
}
