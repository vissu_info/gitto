package com.vissu.accesstogit.access2git.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;

import com.jakewharton.rxbinding.widget.RxSearchView;
import com.vissu.accesstogit.access2git.R;
import com.vissu.accesstogit.access2git.adapter.ReposAdapter;
import com.vissu.accesstogit.access2git.constants.Constants;
import com.vissu.accesstogit.access2git.events.NetworkStatusChangeEvent;
import com.vissu.accesstogit.access2git.models.response.BaseResponse;
import com.vissu.accesstogit.access2git.models.response.Repo;
import com.vissu.accesstogit.access2git.models.response.ResultsResponse;
import com.vissu.accesstogit.access2git.network.ApiClient;
import com.vissu.accesstogit.access2git.utils.AppPref;
import com.vissu.accesstogit.access2git.utils.L;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class SearchActivity extends BaseActivity  {

    @Bind(R.id.results_recycler_view) RecyclerView mResultsRecyclerView;
    @Bind(R.id.search_progress) ProgressBar mSearchProgress;
    @Bind(R.id.empty_layout) View mEmptyLayout;
    @Bind(R.id.no_network_layout) View mNoNetworkLayout;
    @Bind(R.id.error_txt) TextView mErrorTxt;
    private CompositeSubscription mSubscription = new CompositeSubscription();
    private ReposAdapter mResultsAdapter;

    private String mKeyword;
    private List<Repo> mStarredRepos;
    private List<Repo> mSearchRepos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mResultsAdapter = new ReposAdapter(new ArrayList<Repo>());
        mResultsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mResultsRecyclerView.setAdapter(mResultsAdapter);

        fetchStarredRepos();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconified(false);
        searchView.setQueryHint(getString(R.string.search_repos));
        searchView.setLayoutParams(new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT));
        MenuItemCompat.expandActionView(searchItem);
        RxSearchView.queryTextChanges(searchView)
                .debounce(400, TimeUnit.MILLISECONDS)
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getSearchObserver());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_logout) {
            logout();
        }
        return true;
    }

    private void logout() {
        AppPref.get(getApplicationContext()).remove(Constants.AUTHORIZATION_KEY);
        Intent intent = new Intent(this, AuthenticationActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        toast(getString(R.string.message_logout_successful));
        finish();
    }

    private Action1<CharSequence> getSearchObserver() {
        return new Action1<CharSequence>() {
            @Override
            public void call(CharSequence text) {
                L.debug("Searching for " + text.toString());
                search(text.toString());
            }
        };
    }

    private void search(final String keyword) {
        mSearchRepos = new ArrayList<>();
        if (keyword == null || keyword.trim().isEmpty()) {
            showData();
            return;
        }
        this.mKeyword = keyword;
        mSearchProgress.setVisibility(View.VISIBLE);
        mSubscription.add(ApiClient.getInstance(getApplicationContext()).requestSearchRepos(keyword)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<BaseResponse<ResultsResponse>>() {
                    @Override
                    public void onCompleted() {
                        if (keyword.isEmpty() || keyword.equalsIgnoreCase(SearchActivity.this.mKeyword)) {
                            mSearchProgress.setVisibility(View.INVISIBLE);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        toast(getString(R.string.message_failed_api));
                        L.error(e.getMessage(), e);
                        if (keyword.isEmpty() || keyword.equalsIgnoreCase(SearchActivity.this.mKeyword)) {
                            mSearchProgress.setVisibility(View.INVISIBLE);
                        }
                    }

                    @Override
                    public void onNext(BaseResponse<ResultsResponse> response) {
                        if (response != null) {
                            if (TextUtils.isEmpty(response.errorMessage)) {
                                SearchActivity.this.mSearchRepos = response.data.repos;
                                showData();
                            } else {
                                toast(response.errorMessage);
                            }
                        }
                    }
                }));
    }

    private void fetchStarredRepos() {
        showProgress(getString(R.string.message_fetch_repos));
        mSubscription.add(ApiClient.getInstance(getApplicationContext()).requestStarredRepos()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<BaseResponse<List<Repo>>>() {
                    @Override
                    public void onCompleted() {
                        dismissProgress();
                    }

                    @Override
                    public void onError(Throwable e) {
                        L.error(e.getMessage(), e);
                    }

                    @Override
                    public void onNext(BaseResponse<List<Repo>> response) {
                        if (response != null) {
                            if (TextUtils.isEmpty(response.errorMessage)) {
                                mStarredRepos = response.data;
                                for (Repo repo : mStarredRepos) {
                                    repo.isStarred = true;
                                }
                                showData();
                            } else {
                                toast(response.errorMessage);
                            }
                        }
                    }
                }));
    }

    private void showData() {
        if (mSearchRepos != null && !mSearchRepos.isEmpty()) {
            for (Repo repo : mStarredRepos) {
                if (repo.name.toLowerCase().contains(mKeyword.toLowerCase())) {
                    int index = mSearchRepos.indexOf(repo);
                    if (index != -1) {
                        mSearchRepos.get(index).isStarred = true;
                    }
                }
            }
        }
        mResultsAdapter.setData(mSearchRepos == null ? new ArrayList<Repo>() : mSearchRepos);
        if (mSearchRepos == null || mSearchRepos.isEmpty()) {
            mEmptyLayout.setVisibility(View.VISIBLE);
            mErrorTxt.setText(TextUtils.isEmpty(mKeyword) ? getString(R.string.message_start_searching) : getString(R.string.message_no_results));
        } else {
            mEmptyLayout.setVisibility(View.GONE);
        }
    }

    @Subscribe
    public void onEventOnMainThread(Repo repo) {
        if (repo.isStarred) {
            onUnstar(repo);
        } else {
            onStar(repo);
        }
    }

    private void onStar(final Repo repo) {
        showProgress(getString(R.string.message_star_repo));
        mSubscription.add(ApiClient.getInstance(getApplicationContext()).starRepo(repo.fullName)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<BaseResponse<Object>>() {
                    @Override
                    public void onCompleted() {
                        dismissProgress();
                    }

                    @Override
                    public void onError(Throwable e) {
                        toast(getString(R.string.message_failed_api));
                        L.error(e.getMessage(), e);
                    }

                    @Override
                    public void onNext(BaseResponse<Object> response) {
                        toast(getString(R.string.message_star_successful));
                        updateRepoOnAdapter(repo);
                    }
                }));
    }

    private void onUnstar(final Repo repo) {
        showProgress(getString(R.string.message_wait_ustar));
        mSubscription.add(ApiClient.getInstance(getApplicationContext()).unstarRepo(repo.fullName)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<BaseResponse<Object>>() {
                    @Override
                    public void onCompleted() {
                        dismissProgress();
                    }

                    @Override
                    public void onError(Throwable e) {
                        toast(getString(R.string.message_failed_api));
                        L.error(e.getMessage(), e);
                    }

                    @Override
                    public void onNext(BaseResponse<Object> response) {
                        toast(getString(R.string.message_unstar_successful));
                        updateRepoOnAdapter(repo);
                    }
                }));
    }

    private void updateRepoOnAdapter(Repo repo) {
        repo.starCount += repo.isStarred ? -1 : 1;
        repo.isStarred = !repo.isStarred;
        mResultsAdapter.updateRepo(repo);
        if(repo.isStarred) {
            mStarredRepos.add(repo);
        } else {
            mStarredRepos.remove(repo);
        }
    }


    @Subscribe
    public void onEventOnMainThread(NetworkStatusChangeEvent event) {
        if (!event.isConnected) {
            mNoNetworkLayout.setVisibility(View.VISIBLE);
        } else {
            mNoNetworkLayout.setVisibility(View.GONE);
        }
    }
}
