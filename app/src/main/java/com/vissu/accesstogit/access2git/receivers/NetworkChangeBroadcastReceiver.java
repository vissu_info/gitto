package com.vissu.accesstogit.access2git.receivers;

/**
 * Created by vissu on 10/19/16.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.vissu.accesstogit.access2git.events.NetworkStatusChangeEvent;
import com.vissu.accesstogit.access2git.utils.L;
import com.vissu.accesstogit.access2git.utils.NetworkUtils;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by vissu on 5/17/16.
 */
public class NetworkChangeBroadcastReceiver extends BroadcastReceiver {

    public static final String ANDROID_INTENT_ACTION_BOOT_COMPLETED = "android.intent.action.BOOT_COMPLETED";
    public static Boolean isConnected = null;

    @Override
    public void onReceive(final Context context, final Intent intent) {
        L.info("Network connection changed");
        int status = NetworkUtils.getConnectivityStatusString(context);
        if (!intent.getAction().equals(ANDROID_INTENT_ACTION_BOOT_COMPLETED)) {
            if (status == NetworkUtils.NETWORK_STATUS_NOT_CONNECTED && (isConnected == null || isConnected)) {
                isConnected = false;
                EventBus.getDefault().post(new NetworkStatusChangeEvent(isConnected));
            } else if (status != NetworkUtils.NETWORK_STATUS_NOT_CONNECTED && (isConnected == null || !isConnected)) {
                isConnected = true;
                EventBus.getDefault().post(new NetworkStatusChangeEvent(isConnected));
            }

        }
    }
}

