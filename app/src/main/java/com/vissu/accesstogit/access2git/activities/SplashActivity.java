package com.vissu.accesstogit.access2git.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;

import com.vissu.accesstogit.access2git.R;
import com.vissu.accesstogit.access2git.constants.Constants;
import com.vissu.accesstogit.access2git.models.response.BaseResponse;
import com.vissu.accesstogit.access2git.models.response.User;
import com.vissu.accesstogit.access2git.utils.AppPref;

import rx.Subscriber;

public class SplashActivity extends BaseActivity {

    private boolean mIsTimerFinished = false;
    private boolean mIsLoginCheckCompleted = false;
    private boolean mIsLoginSuccessful = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new CountDownTimer(3000, 100) {
            @Override
            public void onFinish() {
                mIsTimerFinished = true;
                navigate();
            }

            @Override
            public void onTick(long millisUntilFinished) {}
        }.start();
        checkAutoLogin();
    }

    private void checkAutoLogin() {
        final String authorization = AppPref.get(getApplicationContext()).getValue(Constants.AUTHORIZATION_KEY);
        if(TextUtils.isEmpty(authorization)) {
            mIsLoginCheckCompleted = true;
            mIsLoginSuccessful = false;
            return;
        }
        login(authorization, new Subscriber<BaseResponse<User>>() {
            @Override
            public void onCompleted() {}

            @Override
            public void onError(Throwable e) {
                mIsLoginCheckCompleted = true;
                mIsLoginSuccessful = false;
                toast(getString(R.string.message_auto_login_failed));
                navigate();
            }

            @Override
            public void onNext(BaseResponse<User> response) {
                mIsLoginCheckCompleted = true;
                if(response != null) {
                    if(TextUtils.isEmpty(response.errorMessage)) {

                        AppPref.get(getApplicationContext()).save(Constants.AUTHORIZATION_KEY, authorization);
                        mIsLoginSuccessful = true;
                        toast(getString(R.string.message_login_success));
                        navigate();
                    } else  {
                        mIsLoginSuccessful = false;
                        toast(response.errorMessage);
                    }
                }
            }
        });
    }

    private void navigate() {
        if(mIsTimerFinished && mIsLoginCheckCompleted) {
            startActivity(new Intent(this,
                    mIsLoginSuccessful ? SearchActivity.class : AuthenticationActivity.class));
            finish();
        }
    }
}
