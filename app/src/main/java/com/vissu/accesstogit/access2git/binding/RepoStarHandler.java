package com.vissu.accesstogit.access2git.binding;

import android.view.View;

import com.vissu.accesstogit.access2git.models.response.Repo;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by vissu on 10/16/16.
 */

public class RepoStarHandler {

    public void onRepoStarChanged(View v, Repo repo) {
        EventBus.getDefault().post(repo);
    }
}
