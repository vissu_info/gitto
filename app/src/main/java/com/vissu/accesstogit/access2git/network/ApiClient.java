package com.vissu.accesstogit.access2git.network;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.vissu.accesstogit.access2git.R;
import com.vissu.accesstogit.access2git.constants.Constants;
import com.vissu.accesstogit.access2git.models.response.BaseResponse;
import com.vissu.accesstogit.access2git.models.response.Repo;
import com.vissu.accesstogit.access2git.models.response.ResultsResponse;
import com.vissu.accesstogit.access2git.models.response.User;
import com.vissu.accesstogit.access2git.utils.AppPref;
import com.vissu.accesstogit.access2git.utils.L;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import rx.Observable;
import rx.Subscriber;
import rx.exceptions.OnErrorFailedException;
import rx.functions.Func1;

/**
 * Created by vissu on 10/14/16.
 */

public class ApiClient {

    private String mAuthenticationUrl = "https://api.github.com/user";
    private String mStarredUrl = "https://api.github.com/user/starred";
    private String mSearchUrlFormat = "https://api.github.com/search/repositories?q=%s";
    private String mStarUnstarUrlFormat = "https://api.github.com/user/starred/%s";

    private static ApiClient instance = new ApiClient();
    private OkHttpClient mOkHttpClient;
    private Serializer mSerializer;
    private Gson mGson;
    private Context mContext;

    private ApiClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        mOkHttpClient = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .connectTimeout(500, TimeUnit.MILLISECONDS)
                .build();
        mSerializer = new Persister();
        mGson = new Gson();
    }

    public static ApiClient getInstance(Context context) {
        instance.mContext = context;
        return ApiClient.instance;
    }

    public Observable<Response> getResponse(Request request) {
        String authorization = AppPref.get(mContext).getValue(Constants.AUTHORIZATION_KEY);
        boolean isAuthorizationHeaderAdded = !TextUtils.isEmpty(request.headers().get(Constants.AUTHORIZATION_KEY));
        final Request interceptedRequest = (!isAuthorizationHeaderAdded && !TextUtils.isEmpty(authorization))
                                            ? request.newBuilder()
                                                .addHeader(Constants.AUTHORIZATION_KEY, authorization)
                                                .build()
                                            : request;

        Observable<Response> observable = Observable.create(new Observable.OnSubscribe<Response>() {
            @Override
            public void call(Subscriber<? super Response> subscriber) {
                try {
                    Response response = mOkHttpClient.newCall(interceptedRequest).execute();
                    subscriber.onNext(response);
                    subscriber.onCompleted();
                } catch (IOException e) {
                    subscriber.onError(e);
                }
            }
        });
        return observable;
    }


    public Observable<BaseResponse<User>> requestAuthentication(String authorization) {
        Request request = new Request.Builder()
                .url(mAuthenticationUrl)
                .addHeader(Constants.AUTHORIZATION_KEY, authorization)
                .get()
                .build();
        return getResponse(request)
                .map(convertJSONResponseToObject(mGson, new TypeToken<User>() {}));
    }


    public Observable<BaseResponse<List<Repo>>> requestStarredRepos() {
        Request request = new Request.Builder()
                .url(mStarredUrl)
                .get()
                .build();
        return getResponse(request)
                .map(convertJSONResponseToObject(mGson, new TypeToken<List<Repo>>() {}));
    }

    public Observable<BaseResponse<ResultsResponse>> requestSearchRepos(String searchTerm) {
        Request request = new Request.Builder()
                .url(String.format(mSearchUrlFormat, searchTerm))
                .get()
                .build();
        return getResponse(request)
                .map(convertJSONResponseToObject(mGson, new TypeToken<ResultsResponse>() {}));
    }

    public Observable<BaseResponse<Object>> starRepo(String fullname) {
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, "");
        Request request = new Request.Builder()
                .url(String.format(mStarUnstarUrlFormat, fullname))
                .put(body)
                .build();
        return getResponse(request)
                .map(convertJSONResponseToObject(mGson, new TypeToken<Object>() {}));
    }

    public Observable<BaseResponse<Object>> unstarRepo(String fullName) {
        Request request = new Request.Builder()
                .url(String.format(mStarUnstarUrlFormat, fullName))
                .delete()
                .build();
        return getResponse(request)
                .map(convertJSONResponseToObject(mGson, new TypeToken<Object>() {}));
    }

    private <T> Func1<Response, BaseResponse<T>> convertJSONResponseToObject(final Gson gson, final TypeToken<T> typeToken) {
        return new Func1<Response, BaseResponse<T>>() {
            @Override
            public BaseResponse<T> call(Response response) {
                try {
                    BaseResponse<T> baseResponse = new BaseResponse<>();
                    baseResponse.statusCode = response.code();
                    switch (response.code()) {
                        case 200 :
                            JsonElement element = gson.fromJson(response.body().string(), JsonElement.class);
                            baseResponse.data = gson.fromJson(element, typeToken.getType());
                            break;
                        case 204 :
                            break;
                        case 401:
                            baseResponse.errorMessage = mContext.getResources().getString(R.string.error_invalid_authentication);
                            break;
                        case 400:
                        case 500:
                            baseResponse.errorMessage = mContext.getResources().getString(R.string.error_failed_server_connection);
                            break;
                        default:
                            baseResponse.errorMessage = response.message();
                            break;
                    }
                    return baseResponse;
                } catch (Exception e) {
                    L.error(e.getMessage(), e);
                    throw new OnErrorFailedException(e);
                }
            }
        };
    }
}
