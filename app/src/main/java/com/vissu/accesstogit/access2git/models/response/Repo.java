package com.vissu.accesstogit.access2git.models.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vissu on 10/14/16.
 */
public class Repo {

    public int id;
    public String name;
    @SerializedName("full_name")
    public String fullName;
    public String description;
    @SerializedName("updated_at")
    public String updatedAt;
    @SerializedName("html_url")
    public String url;
    @SerializedName("stargazers_count")
    public Integer starCount;
    public User owner;
    public boolean isStarred;

    @Override
    public boolean equals(Object repo) {
        return repo instanceof Repo && ((Repo) repo).id == id;
    }

    public String getStarCount() {
        return starCount.toString();
    }
}

