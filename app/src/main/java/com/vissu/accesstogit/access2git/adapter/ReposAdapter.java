package com.vissu.accesstogit.access2git.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vissu.accesstogit.access2git.BR;
import com.vissu.accesstogit.access2git.R;
import com.vissu.accesstogit.access2git.binding.RepoStarHandler;
import com.vissu.accesstogit.access2git.databinding.RepoItemBinding;
import com.vissu.accesstogit.access2git.models.response.Repo;

import java.util.List;

/**
 * Created by vissu on 10/15/16.
 */

public class ReposAdapter extends RecyclerView.Adapter<ReposAdapter.ViewHolder> {
    private List<Repo> mRepos;

    public ReposAdapter(List<Repo> myDataset) {
        mRepos = myDataset;
    }

    public void setData(List<Repo> myDataset) {
        mRepos.clear();
        mRepos.addAll(myDataset);
        notifyDataSetChanged();
    }

    public void updateRepo(Repo repo) {
        int index = mRepos.indexOf(repo);
        if(index != -1) {
            mRepos.set(index, repo);
            notifyDataSetChanged();
        }
    }

    @Override
    public ReposAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.repo_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Repo repo = mRepos.get(position);
        holder.bind(repo);
        holder.getBinding().setVariable(BR.repo, repo);
        holder.getBinding().executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mRepos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private RepoItemBinding binding;

        public ViewHolder(View view) {
            super(view);
            binding = DataBindingUtil.bind(view);
        }

        public void bind(Repo repo) {
            binding.setRepo(repo);
            binding.setHandler(new RepoStarHandler());
        }

        public ViewDataBinding getBinding() {
            return binding;
        }
    }

}