package com.vissu.accesstogit.access2git.models.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vissu on 10/14/16.
 */

public class User {
    @SerializedName("login")
    public String username;
    @SerializedName("url")
    public String url;
    @SerializedName("avatar_url")
    public String avatarUrl;
}
