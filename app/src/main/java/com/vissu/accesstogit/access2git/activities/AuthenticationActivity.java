package com.vissu.accesstogit.access2git.activities;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Process;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;

import com.vissu.accesstogit.access2git.R;
import com.vissu.accesstogit.access2git.constants.Constants;
import com.vissu.accesstogit.access2git.databinding.ActivityAuthenticationBinding;
import com.vissu.accesstogit.access2git.models.databinding.LoginUser;
import com.vissu.accesstogit.access2git.models.response.BaseResponse;
import com.vissu.accesstogit.access2git.models.response.User;
import com.vissu.accesstogit.access2git.utils.AppPref;
import com.vissu.accesstogit.access2git.utils.DataUtils;
import com.vissu.accesstogit.access2git.utils.SystemUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import rx.Subscriber;

public class AuthenticationActivity extends BaseActivity {

    private static final Pattern EMAIL_PATTERN = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$", Pattern.CASE_INSENSITIVE);
    private ActivityAuthenticationBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_authentication);
        mBinding.setLoginUser(new LoginUser());
        mBinding.usernameEt.addTextChangedListener(authWatcher);
        mBinding.passwordEt.addTextChangedListener(authWatcher);
        AppPref.get(getApplicationContext()).remove(Constants.AUTHORIZATION_KEY);
        addAccountsAdapterToViews();
    }

    // this will help in providing email suggestions in username edittext
    private void addAccountsAdapterToViews() {
        if (checkPermission(Manifest.permission.GET_ACCOUNTS, Process.myPid(), Process.myUid()) == PackageManager.PERMISSION_GRANTED) {
            Account[] accounts = AccountManager.get(this).getAccounts();
            Set<String> emailSet = new HashSet<String>();
            for (Account account : accounts) {
                if (EMAIL_PATTERN.matcher(account.name).matches()) {
                    emailSet.add(account.name);
                }
            }
            mBinding.usernameEt.setAdapter(new ArrayAdapter<String>(this,
                    android.R.layout.simple_dropdown_item_1line, new ArrayList<String>(emailSet)));
        }
    }

    public void onLogin(View v) {
        mBinding.getLoginUser().loginExecuted = true;
        if (!SystemUtils.isNetworkConnected(this)) {
            toast(getString(R.string.error_no_internet));
            return;
        } else if(!mBinding.getLoginUser().validate(getResources())) {
            return;
        }

        final String authorization = DataUtils.getBasicAuth(mBinding.getLoginUser().username.get(), mBinding.getLoginUser().password.get());
        showProgress(getString(R.string.message_authenticating));
        login(authorization, new Subscriber<BaseResponse<User>>() {
            @Override
            public void onCompleted() {
                dismissProgress();
            }

            @Override
            public void onError(Throwable e) {
                dismissProgress();
                toast(getString(R.string.message_failed_api));
            }

            @Override
            public void onNext(BaseResponse<User> response) {
                if (response != null) {
                    if (TextUtils.isEmpty(response.errorMessage)) {
                        AppPref.get(getApplicationContext()).save(Constants.AUTHORIZATION_KEY, authorization);
                        navigateToSearchScreen();
                    } else {
                        toast(response.errorMessage);
                    }
                }
            }
        });
    }

    private TextWatcher authWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            mBinding.getLoginUser().validate(getResources());
        }
    };

    private void navigateToSearchScreen() {
        startActivity(new Intent(this, SearchActivity.class));
        finish();
    }
}
