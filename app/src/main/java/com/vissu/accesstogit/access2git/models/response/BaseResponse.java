package com.vissu.accesstogit.access2git.models.response;

/**
 * Created by vissu on 10/16/16.
 */

public class BaseResponse <T> {
    public String status;
    public int statusCode;
    public String errorMessage;
    public T data;
}
