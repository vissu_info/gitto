package com.vissu.accesstogit.access2git.models.databinding;

import android.content.res.Resources;

import com.vissu.accesstogit.access2git.R;
import com.vissu.accesstogit.access2git.binding.BindableString;

/**
 * Created by vissu on 10/14/16.
 */

public class LoginUser {

    public BindableString username = new BindableString();
    public BindableString usernameError = new BindableString();
    public BindableString password = new BindableString();
    public BindableString passwordError = new BindableString();

    public boolean loginExecuted;
    public boolean validate(Resources res) {
        if (!loginExecuted) {
            return true;
        }
        int emailErrorRes = 0;
        int passwordErrorRes = 0;
        if (username.get().isEmpty()) {
            emailErrorRes = R.string.error_username_mandatory;
        }
        if (!username.get().isEmpty() && password.get().isEmpty()) {
            passwordErrorRes = R.string.error_password_mandatory;
        }
        usernameError.set(emailErrorRes != 0 ? res.getString(emailErrorRes) : null);
        passwordError.set(passwordErrorRes != 0 ? res.getString(passwordErrorRes) : null);
        return emailErrorRes == 0 && passwordErrorRes == 0;
    }
}
