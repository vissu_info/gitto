package com.vissu.accesstogit.access2git.activities;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;

import com.vissu.accesstogit.access2git.models.response.BaseResponse;
import com.vissu.accesstogit.access2git.models.response.User;
import com.vissu.accesstogit.access2git.network.ApiClient;

import dmax.dialog.SpotsDialog;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by vissu on 10/13/16.
 */

public class BaseActivity extends AppCompatActivity {

    private CompositeSubscription mSubscription = new CompositeSubscription();
    private AlertDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        mSubscription.unsubscribe();
        super.onDestroy();
    }

    protected void showProgress(String message) {
        mProgressDialog = new SpotsDialog(this, message);
        mProgressDialog.setCancelable(false);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    protected void dismissProgress() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    protected void login(String authorization, Subscriber<BaseResponse<User>> loginSubscriber) {
        mSubscription.add(ApiClient.getInstance(getApplicationContext()).requestAuthentication(authorization)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(loginSubscriber));
    }

    protected void toast(String message) {
        Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT).show();
    }

    protected void toastLong(String message) {
        Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG).show();
    }
}
