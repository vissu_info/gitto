package com.vissu.accesstogit.access2git.models.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by vissu on 10/15/16.
 */

public class ResultsResponse {

    @SerializedName("total_count")
    public Integer totalCount;
    @SerializedName("items")
    public List<Repo> repos;
}
