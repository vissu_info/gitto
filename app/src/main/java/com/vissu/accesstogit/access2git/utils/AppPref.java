package com.vissu.accesstogit.access2git.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by vissu on 10/15/16.
 */

public class AppPref {

    public static final String MyPREFERENCES = "Access2GitPrefs";
    private static AppPref instance;
    private Context mContext;

    public static AppPref get(Context context) {
        if(instance == null) {
            instance = new AppPref();
        }
        instance.mContext = context;
        return instance;
    }

    public void save(String key, String value) {
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getValue(String key) {
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        return sharedpreferences.getString(key, "");
    }

    public void remove(String key) {
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.remove(key);
        editor.commit();
    }
}
